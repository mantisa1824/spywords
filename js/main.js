(function() {

  // проверяем поддержку
  if (!Element.prototype.closest) {

    // реализуем
    Element.prototype.closest = function(css) {
      var node = this;

      while (node) {
        if (node.matches(css)) return node;
        else node = node.parentElement;
      }
      return null;
    };
  }

})();


document.addEventListener('DOMContentLoaded', function(){

	document.querySelector('.list').addEventListener('click', function(e){

		var target = e.target,
			tagName = target.tagName,
			targetLi, liOpen, isOpenFlag;

		if(tagName != 'li'){
			targetLi = target.closest('li');
			if(!targetLi) return;
		}

		isOpenFlag = targetLi.classList.contains('open');

		liOpen = document.querySelector('.list li.open');
		if(liOpen){
			liOpen.classList.remove('open')
			liOpen.classList.add('close');
		}
		
		if(isOpenFlag) return;

		targetLi.classList.remove('close')
		targetLi.classList.add('open');
	});

});