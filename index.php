<?php

$dataXml = file_get_contents(__DIR__. "/data.xml");

$data = array();

$reg = '/chargeOffDate="(.*?)".*payeeName="(.*?)".*purpose="(.*?)"/m';
preg_match_all($reg, $dataXml, $matches, PREG_SET_ORDER, 0);


if(!empty($matches)){

	$page = 0;

	if(isset($_GET['page']) && $_GET['page'] > 1){
		$offset = $_GET['page'] == 2 ? 9 : (($_GET['page'] - 1) * 10) - 1;
	}

	$itemPageMatches = array_slice($matches, $offset, 10);

    foreach($itemPageMatches as $key => $item){

        $data[$key] = array();

        foreach ($item as $subKey => $subItem) {
            
            switch($subKey){
                case 1:
                    $data[$key]['chargeOffDate'] = $subItem;
                    break;
                case 2:
                	$data[$key]['payeeName'] = $subItem; //iconv('windows-1251', 'utf-8', );
                	break;
                case 3:
                	$data[$key]['purpose'] = $subItem; //iconv('windows-1251', 'utf-8', );
                	break;
            }
        }
    }

    $pagesCount = ceil(count($matches) / 10);
    if(!(count($matches) % 10)){
    	++$pagesCount;
    }
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Список платежей</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

	<div class="wrapp">
		
		<?php if(!empty($data)): ?>
		
			<h1>Список платежей</h1>

			<ul class="list">
				
				<?php foreach($data as $item): ?>

					<li class="close">
						
						<div class="paee-name">
							<?=$item['payeeName']?>
						</div>

						<div class="table-data">
							<table>
							<thead>
								<tr>
									<th>Дата платежа:</th>
									<th>Данные платежа:</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<th><?=$item['chargeOffDate']?></th>
									<th><?=$item['purpose']?></th>
								</tr>
							</tbody>
						</table>
						</div>

					</li>

				<?php endforeach; ?>
			</ul>

			<ul class="paginate">
				
				<?php for($i = 1; $i <= $pagesCount; $i++): ?>

					<?php 
						$isActiveClass = $i == $_GET['page'] ? 'class="active"' : null;
					?>

					<li>
						<a <?=$isActiveClass?> href="/spywords/?page=<?=$i?>"><?=$i?></a>
					</li>
				<?php endfor; ?>

			</ul>
			

		<?php else: ?>

		<?php endif; ?>

	</div>


<script src="js/main.js"></script>

</body>
</html>